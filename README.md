# add-todo-app-vue
- This Vue app is a front-end side of modanisa assignment. This app allows you create todos and list them.
- This app connect with backend side which is coded with golang , I use `axios` for http requests.

## Status of Front-end side of Assignment
The project is almost finished. I published my app with DigitalOcean and Ngnix server, in this url : "https://add-todo-app-vue-2-uhymo.ondigitalocean.app/" .

<a href="https://ibb.co/TLR8rBN"><img src="https://i.ibb.co/zJbRNht/vue-publish.png" alt="vue-publish" border="0"></a>

## Components
 UI side consists of 3 components. These are:
- todo
- todolist
- addtodo

## Project setup
```
yarn install
```
- It needs for requiring package implements

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

## Testing
 This project shows Unit Testing , Acceptance Testing and Contract Testing

### Run your unit tests
```
yarn test:unit
```
- Unit tests wrotes with vue cli unit jest 

Unit test like :

<a href="https://ibb.co/k9WzB9r"><img src="https://i.ibb.co/TbFJ2bn/vue-unit-test.png" alt="vue-unit-test" border="0"></a> 

### Run your Acceptance tests
```
yarn test:acceptance
```
- Acceptance tests wrotes with cypress

Acceptance test like :

<a href="https://ibb.co/9c4DRfZ"><img src="https://i.ibb.co/DzMJ36r/vue-e2e-test.png" alt="vue-e2e-test" border="0"></a>

### Run your Contract tests
```
yarn test:pact
```
- Contract tests wrotes with `pact` tool. Pact.io has a tool called `pact-flow` publishing and verifying pacts between provider and consumer

 Pact test like :
 
 <a href="https://ibb.co/6WgHFwz"><img src="https://i.ibb.co/MCMc7Sr/Vue-pact-test.png" alt="Vue-pact-test" border="0"></a>

## CI/CD
I used Gitlab pipeline for this project. Before I installed my local docker vue image to digital ocean server. 
It gives me a server url and i used this url on my gitlab-cli.yml

<a href="https://ibb.co/Px5yBGS"><img src="https://i.ibb.co/sjF4L9B/digitalocean-docker-vue-server.png" alt="digitalocean-docker-vue-server" border="0"></a>

When i pushed any code to my relevant branch gitlab-cli.yml works and updates my url. 

<a href="https://ibb.co/Gvj3bsk"><img src="https://i.ibb.co/QFGj18n/show-pipeline-vue.png" alt="show-pipeline-vue" border="0"></a>


### Notes About Assignment
- Acceptance tests not working on cloud for now. I think problem is opening browser. Headless mode may be help to fix issue. I m still researching about it.


