const pact = require('@pact-foundation/pact-node');
const path = require('path');

// if (!process.env.CI && !process.env.PUBLISH_PACT) {
//     console.log("skipping Pact publish...");
//     return
// }


const gitHash = require('child_process')
    .execSync('git rev-parse --short HEAD')
    .toString().trim();

const opts = {
    pactFilesOrDirs: [path.resolve(`${process.cwd()}/contract`, 'pacts')],
    pactBroker: 'https://ramazan.pactflow.io/',
    pactBrokerToken: 'ntgRrKGTpjVTFkF-nfDj6Q',
    // pactBrokerUsername: pactBrokerUsername,
    // pactBrokerPassword: pactBrokerPassword,
    tags: ['prod', 'test'],
    consumerVersion: gitHash
};

pact
    .publishPacts(opts)
    .then(() => {
        console.log('Pact contract publishing complete!');
        // console.log('');
        // console.log(`Head over to ${pactBrokerUrl} and login with`);
        // console.log(`=> Username: ${pactBrokerUsername}`);
        // console.log(`=> Password: ${pactBrokerPassword}`);
        // console.log('to see your published contracts.')
    })
    .catch(e => {
        console.log('Pact contract publishing failed: ', e)
    });
