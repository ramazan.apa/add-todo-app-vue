import axios from "axios";

const domain =  'https://add-todo-app-go-2-y6tah.ondigitalocean.app';
const baseGetTodoListUrl = `${domain}/api`;
const baseAddTodoUrl = `${domain}/api/add-todo`

const getTodoLisDoRequest = async () => {
    const response = axios.get(baseGetTodoListUrl)
    return await response;
}
 
const addTodoDoRequest = async (body:any) => {
    const response =  axios.post(baseAddTodoUrl,body)
    return await response;
}

export const apis = {getTodoLisDoRequest,addTodoDoRequest}